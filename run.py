from selenium.webdriver.common.by import By
import undetected_chromedriver as uc
import time, os
from selenium.webdriver.common.keys import Keys
import subprocess
import argparse

parser = argparse.ArgumentParser(description="My parser")
parser.add_argument('--headless', dest='headless', default=True, action='store_true')
args = parser.parse_args()

# Resolve the user's home directory and create the path for the data directory
data_dir = os.path.expanduser("~/.chatGPT")

options = uc.ChromeOptions()

# Use the data directory for persistent storage
options.add_argument(f'--user-data-dir={data_dir}')

driver = uc.Chrome(headless=False, use_subprocess=False, options=options)
driver.get('https://chat.openai.com/?model=gpt-4')

time.sleep(5)

outfile = open("gpt4_yield.tsv", "w")

# Search for the element using its XPath and text content
elements = driver.find_elements(By.XPATH, "//h2[contains(text(), 'Get started')]")

# User Login:
# Check if the element is present
if len(elements) > 0:
    _ = input("You don't seem to be logged in. Please log in now and press ENTER when you're done! If no browser window is present, start with --headless")
else:
    print("User is likely to be logged in. Continuing..")

while True:
    for _ in range(50):

        driver.get('https://chat.openai.com/?model=gpt-4')

        # Execute the shell command and capture the output
        completed_process = subprocess.run("""shuf -n 15 ids-sst-train.csv | awk -F'\t' '{print $3 "\t" $4}'""",
                                        shell=True,
                                        stdout=subprocess.PIPE,
                                        stderr=subprocess.PIPE,
                                        text=True)  # text=True to get string output rather than bytes

        # Get the stdout (standard output) from the completed process
        example_data = completed_process.stdout.strip()

        prompt = """For building a sentiment classifier, I need highly educational examples. I will provide you with some examples, please continue to give me examples on the same level of quality and average complexity. Please also respect the quirky formatting and return them as tab-seperated:

    {example_data}""".format(example_data=example_data.replace("\t", "___"))

        textarea = driver.find_element(By.ID, "prompt-textarea")    
        driver.execute_script("arguments[0].value = arguments[1];", textarea, prompt)
        textarea.send_keys(' ')
        textarea.send_keys(Keys.BACKSPACE)

        # Send return key
        # Locate the button using CSS selector
        button = driver.find_element(By.CSS_SELECTOR, '[data-testid="send-button"]')

        # Click the button
        button.click()

        #_ = input("wait")
        time.sleep(120)

        result = driver.find_element(By.CLASS_NAME, 'markdown').text
        

        for line in result.split("\n"):
            if not "___" in line:
                continue
            try:
                example = line.split("___")[0]
                rating = int(line.split("___")[1])
            except:
                continue

            
            print("SYNTHETIC", "GPT4", example, rating, sep="\t", file=outfile)
            print("SYNTHETIC", "GPT4", example, rating, sep="\t")

        
    time.sleep(60*60*3)
